import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.RequestDispatcher;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

//@WebServlet(urlPatterns = {"/welcome","/new","/insert","/delete","/edit","/update"})
@MultipartConfig
public class DiaryController extends HttpServlet {
    private DiaryDao diaryDao;

    public void init() {
        String jdbcURL = "jdbc:postgresql://ec2-34-237-166-54.compute-1.amazonaws.com:5432/d92chavk88aa8f?user=rhpseosuxgixoc&password=49b4aebe8939feacb77a23d8ae3f089de74e7e682c34e14d3607167deeeb582b";
        String jdbcUsername = "rhpseosuxgixoc";
        String jdbcPassword = "49b4aebe8939feacb77a23d8ae3f089de74e7e682c34e14d3607167deeeb582b";

        diaryDao = new DiaryDao(jdbcURL, jdbcUsername, jdbcPassword);

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getServletPath();
        System.out.println(action);

        try {
            switch (action) {
                case "/new":
                    showNewForm(request, response);
                    break;
                case "/edit":
                    showEditForm(request, response);
                    break;
                case "/insert":
                    insertDiary(request, response);
                    break;
                case "/delete":
                    deleteDiary(request, response);
                    break;
                case "/update":
                    updateDiary(request, response);
                    break;
                case "/welcome":
                    listDiary(request, response);
                    break;
                case "/search":
                    searchDiary(request, response);
                    break;
                case "/redirect":
                    redirect(request, response);
                    break;
//                default:
//                    listDiary(request, response);
//                    break;
            }
        } catch (SQLException ex) {
            throw new ServletException(ex);
        }
    }


    private void redirect(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String search = request.getParameter("searchTerm");
        System.out.println(search);
        request.setAttribute("search", search);

        RequestDispatcher dispatcher = request.getRequestDispatcher("search.jsp");
        dispatcher.forward(request, response);
    }


    private void searchDiary(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {

        String searchTerm = request.getParameter("search");
        List<Diary> listDiary = diaryDao.searchDiary(searchTerm);
        System.out.println("The list: " + listDiary);
        JSONResponse.send(response, listDiary);

    }

    private void listDiary(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException, ServletException {

        HttpSession session = request.getSession();
        String uname = (String) session.getAttribute("username");
        List<Diary> listDiary = diaryDao.listUserEntries(uname);
        request.setAttribute("listDiary", listDiary);
        RequestDispatcher dispatcher = request.getRequestDispatcher("welcome.jsp");
        dispatcher.forward(request, response);

    }

    private void showNewForm(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("DiaryForm.jsp");
        dispatcher.forward(request, response);

    }

    private void showEditForm(HttpServletRequest request, HttpServletResponse response) throws SQLException, ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        Diary existingDiary = diaryDao.getDiary(id);
        RequestDispatcher dispatcher = request.getRequestDispatcher("DiaryForm.jsp");
        request.setAttribute("diary", existingDiary);
        dispatcher.forward(request, response);
    }

    private void insertDiary(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
        HttpSession session = request.getSession();

        String title = request.getParameter("title");
        String content = request.getParameter("content");
        String uname = (String) session.getAttribute("username");


        Diary newDiary = new Diary(title, uname, content);
        diaryDao.insertDiary(newDiary);

        List<Part> fileParts = request.getParts().stream().filter(part -> "file".equals(part.getName()) && part.getSize() > 0).collect(Collectors.toList()); // Retrieves <input type="file" name="file" multiple="true">

        for (Part filePart : fileParts) {
            String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString(); // MSIE fix.
            InputStream fileContent = filePart.getInputStream();
            File uploads = new File("/Users/Chris/IntelliJ/CRUDAPP/fs");
            File file = new File(uploads, fileName);
            try (InputStream input = filePart.getInputStream()) {
                Files.copy(input, file.toPath());
            }
        }

        System.out.println("file uploaded");

        response.sendRedirect("welcome");


//        try {
//
//            ServletFileUpload sf = new ServletFileUpload(new DiskFileItemFactory());
//
//            List<FileItem> multiFiles = sf.parseRequest(request);
//
//            for (FileItem item : multiFiles) {
//                item.write(new File("/Users/Chris/IntelliJ/CRUDAPP/fs" + item.getName()));
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


    }

    private void deleteDiary(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
        int id = Integer.parseInt(request.getParameter("id"));

        Diary diary = new Diary(id);
        diaryDao.deleteDiary(diary);
        response.sendRedirect("welcome");


    }

    private void updateDiary(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
        HttpSession session = request.getSession();

        int id = Integer.parseInt(request.getParameter("id"));
        String title = request.getParameter("title");
        String uname = (String) session.getAttribute("username");
        String content = request.getParameter("content");

        Diary diary = new Diary(id, title, uname, content);
        diaryDao.updateDiary(diary);
        response.sendRedirect("welcome");

    }


}
