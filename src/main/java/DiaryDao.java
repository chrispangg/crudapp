import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DiaryDao {
    private final String jdbcURL;
    private final String jdbcUsername;
    private final String jdbcPassword;
    private Connection jdbcConnection;

    public DiaryDao(String jdbcURL, String jdbcUsername, String jdbcPassword) {
        this.jdbcURL = jdbcURL;
        this.jdbcUsername = jdbcUsername;
        this.jdbcPassword = jdbcPassword;
    }

    protected void connect() throws SQLException {
        if (jdbcConnection == null || jdbcConnection.isClosed()) {
            try {
                Class.forName("org.postgresql.Driver");
            } catch (ClassNotFoundException e) {
                throw new SQLException(e);
            }
            jdbcConnection = DriverManager.getConnection(jdbcURL, jdbcUsername, jdbcPassword);
        }
    }

    protected void disconnect() throws SQLException {
        if (jdbcConnection != null && !jdbcConnection.isClosed()) {
            jdbcConnection.close();
        }
    }

    public List<Diary> listUserEntries(String uname) throws SQLException {
        List<Diary> listEntries = new ArrayList<>();

        String sql = "SELECT * FROM record WHERE uname = ? ";
        connect();
        PreparedStatement statement = jdbcConnection.prepareStatement(sql);
        statement.setString(1, uname);
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            String title = resultSet.getString("title");
            String content = resultSet.getString("content");
            Diary diary = new Diary(id, title, uname, content);
            diary.setContentWithNoTags(content);
            listEntries.add(diary);
        }

        return listEntries;
    }

    public boolean insertDiary(Diary diary) throws SQLException {
        String sql = "INSERT INTO record (title, content, uname) VALUES (?, ?, ?)";
        connect();
        PreparedStatement st = jdbcConnection.prepareStatement(sql);

        st.setString(1, diary.getTitle());
        st.setString(2, diary.getContent());
        st.setString(3, diary.getUname());

        boolean rowInserted = st.executeUpdate() > 0;
        st.close();
        disconnect();
        return rowInserted;

    }

    public boolean deleteDiary(Diary diary) throws SQLException {
        String sql = "DELETE FROM record WHERE id = ?";
        connect();
        PreparedStatement statement = jdbcConnection.prepareStatement(sql);

        statement.setInt(1, diary.getId());

        boolean rowDeleted = statement.executeUpdate() > 0;
        statement.close();
        disconnect();
        return rowDeleted;

    }

    public boolean updateDiary(Diary diary) throws SQLException {
        String sql = "UPDATE record SET title = ?, content = ? WHERE id = ?";
        connect();
        PreparedStatement statement = jdbcConnection.prepareStatement(sql);
        statement.setString(1, diary.getTitle());
        statement.setString(2, diary.getContent());
        statement.setInt(3, diary.getId());

        boolean rowUpdated = statement.executeUpdate() > 0;
        statement.close();
        disconnect();
        return rowUpdated;

    }

    public Diary getDiary(int id) throws SQLException {
        Diary diary = null;
        String sql = "SELECT * FROM record WHERE id = ?";
        connect();
        PreparedStatement statement = jdbcConnection.prepareStatement(sql);
        statement.setInt(1, id);
        ResultSet resultSet = statement.executeQuery();

        if (resultSet.next()) {
            String title = resultSet.getString("title");
            String uname = resultSet.getString("uname");
            String content = resultSet.getString("content");

            diary = new Diary(id, title, uname, content);
        }

        resultSet.close();
        statement.close();

        return diary;

    }

    public List<Diary> searchDiary(String search) throws SQLException {
        List<Diary> diaryList = new ArrayList<>();
        search = "'%" + search + "%'";
        System.out.println("The search term is: " + search);
//        String sql = "SELECT * FROM record WHERE content like ? ";
        String sql = "select * from record where content like ?";
        System.out.println("about to connect");
        connect();
        System.out.println("connected");
        PreparedStatement statement = jdbcConnection.prepareStatement(sql);
        System.out.println("statement prepared");
        statement.setString(1, search);
        ResultSet resultSet = statement.executeQuery();
        System.out.println("ResultSet Object created");

        while (resultSet.next()) {
            int id = resultSet.getInt(1);
            String title = resultSet.getString(2);
            String content = resultSet.getString(3);
            String uname = resultSet.getString(4);
            Diary diary = new Diary();
            diary.setId(id);
            diary.setTitle(title);
            diary.setContentWithNoTags(content);
            diary.setUname(uname);

            diaryList.add(diary);
        }

        System.out.println("Printing diary at searchDiary DAO: " + diaryList);


        return diaryList;
    }

}
