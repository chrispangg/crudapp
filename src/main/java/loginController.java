import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

@WebServlet(urlPatterns = {"/login","/logout", "/create"})
public class loginController extends HttpServlet {

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String action = request.getServletPath();
        System.out.println("loginController: " + action);

        switch (action) {
            case "/login":
                login(request, response);
                break;
            case "/logout":
                logout(request, response);
                break;
            case "/create":
                create(request, response);
                break;
        }
    }

    public void login(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String uname = request.getParameter("uname");
        String pass = request.getParameter("pass");

        LoginDao dao = new LoginDao();

        if (dao.check(uname, pass)) {
            HttpSession session = request.getSession();
            session.setAttribute("username", uname);

            response.sendRedirect("/welcome");


        } else {
            response.sendRedirect("index.jsp");
            System.out.println("cannot forward");

        }

    }

    private void create(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String uname = request.getParameter("uname");
        String pass = request.getParameter("pass");
        System.out.println("on the createAcc method");
        PrintWriter out = response.getWriter();


        LoginDao dao = new LoginDao();
        if(dao.accountCheck(uname, pass)){
            HttpSession session = request.getSession();
            session.setAttribute("username", uname);
            System.out.println("Account was created successfully");
            out.println("<script type=\"text/javascript\">");
            out.println("alert('Account was created successfully');");

            out.println("location='/welcome';");
            out.println("</script>");
//            response.sendRedirect("/index.jsp");

        } else {
            System.out.println("Account was NOT created successfully");

//            response.sendRedirect("index.jsp");
            out.println("<script type=\"text/javascript\">");
            out.println("alert('Account was NOT created successfully');");
            out.println("location='index.jsp';");
            out.println("</script>");




        }

    }

    protected void logout(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession();
        session.removeAttribute("username");
        session.invalidate();
        resp.sendRedirect("/");
    }

}

