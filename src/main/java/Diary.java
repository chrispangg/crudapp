import java.io.Serializable;

public class Diary implements Serializable {

    protected int id;
    protected String title;
    protected String uname;
    protected String content;

    public Diary() {
    }

    public Diary(int id) {
        this.id = id;
    }

    public Diary(String uname) {
        this.uname = uname;
    }

    public Diary(int id, String title, String uname) {
        this.id = id;
        this.title = title;
        this.uname = uname;
    }

    public Diary(String title, String uname, String content) {
        this.title = title;
        this.uname = uname;
        this.content = content;
    }

    public Diary(int id, String title, String uname, String content) {
        this.id = id;
        this.title = title;
        this.uname = uname;
        this.content = content;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getContent() {
        return content;
    }

    public void setContentWithNoTags(String content) {

        String contentNoTags = null;
        if (content != null) {
            contentNoTags = content.replaceAll("\\<.*?\\>", "");
            System.out.println("content is set with no tags now");
            this.content = contentNoTags;
        } else {
            this.content = null;
        }
    }

    public void setContent(String content) {
        this.content = content;
    }
}
