window.addEventListener("load", () => {

    function loadList(list) {
        let body = document.querySelector("#body");

        for (let item of list) {
            body.innerHTML += `
            <h2>${item.id} + ${item.title} </h2>
            <h3>${item.content}</h3>
            <br>
            `;
        }
    }

    async function fetchData() {
        let response = await fetch("/search?content=123");

        let search = await response.json();
        console.log(search);

        loadList(search);
    }

    fetchData();

});