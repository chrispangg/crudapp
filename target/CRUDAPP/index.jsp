
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%--<script type="text/javascript" src="./${pageContext.request.contextPath}/resources/script.js"></script>--%>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/script.js"></script>
<link type="text/css" rel="stylesheet" href="<%=request.getContextPath()%>/resources/style.css" >

<%--"<%request.getContextPath();%>--%>
<html>
<body>
<header id="header">Welcome to Chris' Diary</header>


<div class="login-form">
    <form action="login" method="post">
        <h2 class="text-center">Log in</h2>
        <div class="form-group">
            <input type="text" class="form-control" name="uname" required="required">
        </div>
        <div class="form-group">
            <input type="password" class="form-control" name="pass" required="required">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-block">Log in</button>
        </div>

    </form>
    <button class="text-center" id="createAccButton">Create an Account</button>
</div>

<div id="signup" class="login-form hide">
    <form action="create" method="post">
        <h2 class="text-center">Create an Account</h2>
        <div class="form-group">
             <input type="text" placeholder="Username"  class="form-control" name="uname" required="required">
        </div>
        <div class="form-group">
          <input type="password" placeholder="Password" class="form-control" name="pass"  required="required">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-block" value="Create Account">Create</button>
        </div>

    </form>

<%--<h3>Login</h3>--%>
<%--<form action="login" method="post">--%>
<%--    Name:<input type="text" name="uname"/><br/><br/>--%>
<%--    Password:<input type="password" name="pass"/><br/><br/>--%>
<%--    <input type="submit" value="login" id="submit">--%>
<%--</form>--%>

<div  class="hide">
<h3>Create a new account</h3>
<form action="create" method="post">
    Username:<input type="text" placeholder="Username" name="uname"/><br/><br/>
    Password:<input type="password" placeholder="Password" name="pass"/><br/><br/>
    <input type="submit" value="Create Account">
</form>
</div>

</body>

</html>
