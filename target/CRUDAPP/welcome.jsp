<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link type="text/css" rel="stylesheet" href="<%=request.getContextPath()%>/resources/style.css" >
<%@ page isELIgnored="false" %>

<html>
<head>
    <title>Welcome</title>
</head>
<body>
<%

    response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
    response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
    response.setHeader("Expires", "0"); // Proxies.

    if (session.getAttribute("username") == null) {
        response.sendRedirect("index.jsp");
    }

%>

<h1>This is the Welcome Page</h1>
<h2>Welcome to your diary, ${username}!</h2>


<form action="redirect" method="post">
    <input type="text" name="searchTerm">
    <input type="submit" value="search">
</form>

<form action="logout" method="post">
    <input type="submit" value="logout">
</form>

<h2>
    <a href="new">Add New Entries</a>
    &nbsp;&nbsp;&nbsp;
    <a href="welcome">List All Entries</a>
</h2>
<div id="table">
    <table>
        <caption><h2>List of Diary Entries</h2></caption>
        <tr>
            <th>ID</th>
            <th>User</th>
            <th>Title</th>
            <th>Content</th>
            <th>Actions</th>
        </tr>
        <c:forEach var="diary" items="${listDiary}">
            <tr>
                <td><c:out value="${diary.id}"/></td>
                <td><c:out value="${diary.uname}"/></td>
                <td><c:out value="${diary.title}"/></td>
                <td><c:out value="${diary.content}"/></td>
                <td>
                    <a href="/edit?id=<c:out value='${diary.id}' />">Edit</a>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="/delete?id=<c:out value='${diary.id}' />">Delete</a>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>


</body>
</html>
