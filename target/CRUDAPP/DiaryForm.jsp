<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>

<html>
<script src='https://cdn.tiny.cloud/1/cqrrsk4j91lr08vk4ks5m307ia9g2oy94j66jztou9f1wmto/tinymce/5/tinymce.min.js'
        referrerpolicy="origin">
</script>

<head>
    <title>Title</title>
</head>
<script>
    tinymce.init({
        selector: 'textarea#mytextarea'
    });
</script>
<html lang="en">
<body>
<h1>Diary Management</h1>
<h2>
    <a href="new">Add New Diary Log</a>
    &nbsp;&nbsp;&nbsp;
    <a href="welcome">List All Diaries</a>

</h2>
<div>
    <c:if test="${diary != null}">
    <form action="update" method="post" enctype="multipart/form-data">
        </c:if>
        <c:if test="${diary == null}">
        <form action="insert" method="post" enctype="multipart/form-data">
            </c:if>
            <table>
                <caption>
                    <h2>
                        <c:if test="${diary != null}">
                            Edit Diary Log
                        </c:if>
                        <c:if test="${diary == null}">
                            Add New Diary Log
                        </c:if>
                    </h2>
                </caption>
                <c:if test="${diary != null}">
                    <input type="hidden" name="id" value="<c:out value='${diary.id}' />"/>
                </c:if>
                <tr>
                    <th>Title:</th>
                    <td>
                        <input type="text" name="title" size="45"
                               value="<c:out value='${diary.title}' />"
                        />
                    </td>
                </tr>
                <tr>
                    <th>Content:</th>
                    <td>
                        <textarea id="mytextarea" name="content" >
                            <c:out value='${diary.content}'/>
                        </textarea>
<%--                        <input type="text" name="content" size="45"--%>
<%--                               value="<c:out value='${diary.content}' />"--%>
<%--                        />--%>
                    </td>
                <tr>
                    <td>
                        <input type="file" name="file"/>
                        <input type="submit" value="Save"/>
                    </td>
                </tr>

            </table>
        </form>


</body>
</html>
